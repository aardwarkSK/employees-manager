import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { NewComponent } from './new/new.component';

const appRoutes: Routes = [{
  path: 'list',
  component: EmployeesListComponent
}, {
  path: 'list/new',
  component: NewComponent
}, {
  path: '',
  redirectTo: '/list',
  pathMatch: 'full'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppCustomRoutingModule { }
