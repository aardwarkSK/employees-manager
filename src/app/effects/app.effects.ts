import { Injectable } from '@angular/core';
import { Actions, Effect, createEffect, ROOT_EFFECTS_INIT, ofType } from '@ngrx/effects';
import {DeleteEmployeeEntry, EmployeeEntryActionTypes, LoadEmployeeEntrys} from '../store/employee-entry/employee-entry.actions';
import {map, mergeMap} from 'rxjs/operators';
import {EmployeeListService} from '../employee-list.service';



@Injectable()
export class AppEffects {

  constructor(private actions$: Actions, private employeeListService: EmployeeListService) { }

  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROOT_EFFECTS_INIT),
      mergeMap(element => this.employeeListService.getAllEmployees()),
        map(employeeEntrys => new LoadEmployeeEntrys({employeeEntrys}))
    )
  );

  @Effect({dispatch: false})
  delete$ = this.actions$.pipe(
      ofType(EmployeeEntryActionTypes.DeleteEmployeeEntry),
      mergeMap((action: DeleteEmployeeEntry) => this.employeeListService.deleteEmployee(action.payload.id))
  );
}
