import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {EmployeeEntry} from './store/employee-entry/employee-entry.model';
import {delay} from 'rxjs/operators';
import {element} from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class EmployeeListService {
  private mockEmpoyeeEntry: EmployeeEntry[] = [{
      name: 'Jozo Pucik',
      id: '1234',
      role: 'Senior',
      hireDate: new Date(1975, 2, 15)
    }, {
      name: 'Peter Mrkva',
      id: '9874',
      role: 'Junior Programmer',
      hireDate: new Date(2015, 7, 27)
    }];
  private roleMock: string[] = ['Junior', 'Senior'];

  constructor() { }
  public getAllEmployees(): Observable<EmployeeEntry[]> {
    return of(this.mockEmpoyeeEntry).pipe(delay(5000));
  }

  public deleteEmployee(id: string): Observable<boolean> {
    const index = this.mockEmpoyeeEntry.findIndex(element => element.id === id);
    if (index >= 0) {
      this.mockEmpoyeeEntry = this.mockEmpoyeeEntry.filter(element => element.id !== id);
    }
    return of(index >= 0);
  }

  public getRoleList(): Observable<string[]> {
    return of(this.roleMock);
  }
}
