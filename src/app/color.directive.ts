import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {

  @HostListener('mouseenter') onMouseEnter() {
    this.changeBackgroundColor('yellow');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.changeBackgroundColor('white');
  }

  constructor(private elementRef: ElementRef) { }

  changeBackgroundColor(color: string) {
    this.elementRef.nativeElement.style.backgroundColor = color;
  }

}
