import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeEntry } from '../store/employee-entry/employee-entry.model';

@Component({
  selector: 'app-employee-entry',
  templateUrl: './employee-entry.component.html',
  styleUrls: ['./employee-entry.component.css']
})
export class EmployeeEntryComponent implements OnInit {

  @Input() employee: EmployeeEntry;
  @Output() delete: EventEmitter<EmployeeEntry> =  new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
