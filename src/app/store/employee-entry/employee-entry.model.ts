export interface EmployeeEntry {
  name: string;
  id: string;
  role: string;
  hireDate: Date;
}
