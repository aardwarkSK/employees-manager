import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { EmployeeEntry } from './employee-entry.model';

export enum EmployeeEntryActionTypes {
  LoadEmployeeEntrys = '[EmployeeEntry] Load EmployeeEntrys',
  AddEmployeeEntry = '[EmployeeEntry] Add EmployeeEntry',
  UpsertEmployeeEntry = '[EmployeeEntry] Upsert EmployeeEntry',
  AddEmployeeEntrys = '[EmployeeEntry] Add EmployeeEntrys',
  UpsertEmployeeEntrys = '[EmployeeEntry] Upsert EmployeeEntrys',
  UpdateEmployeeEntry = '[EmployeeEntry] Update EmployeeEntry',
  UpdateEmployeeEntrys = '[EmployeeEntry] Update EmployeeEntrys',
  DeleteEmployeeEntry = '[EmployeeEntry] Delete EmployeeEntry',
  DeleteEmployeeEntrys = '[EmployeeEntry] Delete EmployeeEntrys',
  ClearEmployeeEntrys = '[EmployeeEntry] Clear EmployeeEntrys'
}

export class LoadEmployeeEntrys implements Action {
  readonly type = EmployeeEntryActionTypes.LoadEmployeeEntrys;

  constructor(public payload: { employeeEntrys: EmployeeEntry[] }) {}
}

export class AddEmployeeEntry implements Action {
  readonly type = EmployeeEntryActionTypes.AddEmployeeEntry;

  constructor(public payload: { employeeEntry: EmployeeEntry }) {}
}

export class UpsertEmployeeEntry implements Action {
  readonly type = EmployeeEntryActionTypes.UpsertEmployeeEntry;

  constructor(public payload: { employeeEntry: EmployeeEntry }) {}
}

export class AddEmployeeEntrys implements Action {
  readonly type = EmployeeEntryActionTypes.AddEmployeeEntrys;

  constructor(public payload: { employeeEntrys: EmployeeEntry[] }) {}
}

export class UpsertEmployeeEntrys implements Action {
  readonly type = EmployeeEntryActionTypes.UpsertEmployeeEntrys;

  constructor(public payload: { employeeEntrys: EmployeeEntry[] }) {}
}

export class UpdateEmployeeEntry implements Action {
  readonly type = EmployeeEntryActionTypes.UpdateEmployeeEntry;

  constructor(public payload: { employeeEntry: Update<EmployeeEntry> }) {}
}

export class UpdateEmployeeEntrys implements Action {
  readonly type = EmployeeEntryActionTypes.UpdateEmployeeEntrys;

  constructor(public payload: { employeeEntrys: Update<EmployeeEntry>[] }) {}
}

export class DeleteEmployeeEntry implements Action {
  readonly type = EmployeeEntryActionTypes.DeleteEmployeeEntry;

  constructor(public payload: { id: string }) {}
}

export class DeleteEmployeeEntrys implements Action {
  readonly type = EmployeeEntryActionTypes.DeleteEmployeeEntrys;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearEmployeeEntrys implements Action {
  readonly type = EmployeeEntryActionTypes.ClearEmployeeEntrys;
}

export type EmployeeEntryActions =
 LoadEmployeeEntrys
 | AddEmployeeEntry
 | UpsertEmployeeEntry
 | AddEmployeeEntrys
 | UpsertEmployeeEntrys
 | UpdateEmployeeEntry
 | UpdateEmployeeEntrys
 | DeleteEmployeeEntry
 | DeleteEmployeeEntrys
 | ClearEmployeeEntrys;
