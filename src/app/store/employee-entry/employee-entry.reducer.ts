import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { EmployeeEntry } from './employee-entry.model';
import { EmployeeEntryActions, EmployeeEntryActionTypes } from './employee-entry.actions';

export const employeeEntriesFeatureKey = 'employeeEntries';

export interface State extends EntityState<EmployeeEntry> {
  // additional entities state properties
}

export const adapter: EntityAdapter<EmployeeEntry> = createEntityAdapter<EmployeeEntry>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: EmployeeEntryActions
): State {
  switch (action.type) {
    case EmployeeEntryActionTypes.AddEmployeeEntry: {
      return adapter.addOne(action.payload.employeeEntry, state);
    }

    case EmployeeEntryActionTypes.UpsertEmployeeEntry: {
      return adapter.upsertOne(action.payload.employeeEntry, state);
    }

    case EmployeeEntryActionTypes.AddEmployeeEntrys: {
      return adapter.addMany(action.payload.employeeEntrys, state);
    }

    case EmployeeEntryActionTypes.UpsertEmployeeEntrys: {
      return adapter.upsertMany(action.payload.employeeEntrys, state);
    }

    case EmployeeEntryActionTypes.UpdateEmployeeEntry: {
      return adapter.updateOne(action.payload.employeeEntry, state);
    }

    case EmployeeEntryActionTypes.UpdateEmployeeEntrys: {
      return adapter.updateMany(action.payload.employeeEntrys, state);
    }

    case EmployeeEntryActionTypes.DeleteEmployeeEntry: {
      return adapter.removeOne(action.payload.id, state);
    }

    case EmployeeEntryActionTypes.DeleteEmployeeEntrys: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case EmployeeEntryActionTypes.LoadEmployeeEntrys: {
      return adapter.addAll(action.payload.employeeEntrys, state);
    }

    case EmployeeEntryActionTypes.ClearEmployeeEntrys: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
