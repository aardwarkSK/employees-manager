import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
  Store
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromEmployeeEntry from './employee-entry/employee-entry.reducer';

export const selectEmployeeList = (state: State) => state.employeeListEntry;

export interface State {

  employeeListEntry: fromEmployeeEntry.State;
}

export const reducers: ActionReducerMap<State> = {

  employeeListEntry: fromEmployeeEntry.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
