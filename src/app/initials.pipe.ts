import { Pipe, PipeTransform } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';

@Pipe({
  name: 'initials'
})
export class InitialsPipe implements PipeTransform {

  transform(value: string, upperCase: boolean = false): string {
    let retValue = '';
    let words: string[];

    words = value.split(' ');

    words.forEach(element => {
      retValue += (element.charAt(0));
    });

    retValue = upperCase ? retValue.toUpperCase() : retValue.toLowerCase();
    return retValue;
  }

}
